import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

for i in range(10):
    reg0 = hw.getNode("data_lo").write(0xDEAD0000 + i + 10)
    hw.dispatch()
    reg1 = hw.getNode("data_hi").write(0xCAFE0000 + i +11)
    hw.dispatch()

hw.getNode("control_reg.send").write(1)
hw.dispatch()

print("sent data via control register")

hw.getNode("control_reg.send").write(0)
hw.dispatch()

