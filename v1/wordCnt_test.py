import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg7 = hw.getNode("wrd_cnt_reg").read()
hw.dispatch()

print("wrd_cnt_reg = ", hex(reg7))

hw.getNode("control_reg.frm_src").write(1)
hw.dispatch()

print("Writing 0x1 to control_reg.frm_src")

reg7 = hw.getNode("wrd_cnt_reg").read()
hw.dispatch()

upper = 0xC0FFEE00
print("now wrd_cnt_reg = ", hex(reg7))
print("\nwriting:")
for n in range(5):
    hw.getNode("data_lo_reg").write(0xDEADBEEF)
    hw.dispatch()
    hw.getNode("data_hi_reg").write(0xC0FFEE00 + (2*n))
    hw.dispatch()
    print("0xDEADBEEF and ", hex(upper + (2*n)))

 
reg7 = hw.getNode("wrd_cnt_reg").read()
hw.dispatch()
print("now wrd_cnt_reg = ", hex(reg7))

print("sending data")
hw.getNode("control_reg.frm_src").write(1)
hw.dispatch

hw.getNode("control_reg.frm_src").write(0)
hw.dispatch

reg7 = hw.getNode("wrd_cnt_reg").read()
hw.dispatch()

print("now wrd_cnt_reg = ", hex(reg7))
