import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)


reg5 = hw.getNode("A.regA").read()
hw.dispatch()
reg6 = hw.getNode("B.regB").read()
hw.dispatch()
reg7 = hw.getNode("C.regC").read()
hw.dispatch()
print("reg A = ", reg5)
print("reg B = ", reg6)
print("reg C = ", reg7)
print("srtm test done")
