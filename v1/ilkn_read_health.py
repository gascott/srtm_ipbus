import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg0 = hw.getNode("interlaken_health_lanes").read()
hw.dispatch()
print("interlaken_health_lanes = ", hex(reg0))

print('done!')
