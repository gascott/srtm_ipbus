import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
#print("device_id = ", device_id)

deviceuri = hw.uri()
#print("deviceuri = ", deviceuri)

# read current word count
reg1 = hw.getNode("felix_max_words").read()
hw.dispatch()
print ('felix_max_words = ',hex(reg1))

# now send                                                                     
hw.getNode("felix_control_reg").write(0x11) 
hw.dispatch() 
print ('set felix_control_reg to 0x11')

# check word count again
reg1 = hw.getNode("felix_max_words").read()
hw.dispatch()
print ('felix_max_words = ',hex(reg1))

reg1 = hw.getNode("felix_control_reg").read()
hw.dispatch()
print ('felix_control_reg = ',hex(reg1))
