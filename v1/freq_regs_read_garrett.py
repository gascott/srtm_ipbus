import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

### Set maximum number of counts in the control register
#max_counts = 251658240
#max_counts= 50000000
max_counts= 6000000
#251658240 = f000000 
hw.getNode("freq_counter_control_reg.freq_counter_max_value").write(max_counts)
hw.dispatch()
print("writing max counts ", hex(max_counts))

counts = hw.getNode("freq_counter_control_reg.freq_counter_max_value").read()
hw.dispatch()
print('max counts: ', hex(counts))

### Set the Enable bit in the control register
enRead = hw.getNode("freq_counter_control_reg.freq_counter_enable").read()
hw.dispatch()
print('enable: ', hex(enRead))

enable = 1
hw.getNode("freq_counter_control_reg.freq_counter_enable").write(enable)
hw.dispatch()
print('writing enable bit')

enRead = hw.getNode("freq_counter_control_reg.freq_counter_enable").read()
hw.dispatch()
print('enable: ', hex(enRead))


### Set Clear bit in Frequency control register
read_clear = hw.getNode("freq_counter_control_reg.freq_counter_clear").read()
hw.dispatch()
print("clear: ", hex(read_clear))

clear = 0
hw.getNode("freq_counter_control_reg.freq_counter_clear").write(clear)
hw.dispatch()
print("writing clear bit: ", clear)

read_clear = hw.getNode("freq_counter_control_reg.freq_counter_clear").read()
hw.dispatch()
print("clear: ", hex(read_clear))


### Wait until the done bit is set to 1
done = hw.getNode("freq_counter_control_reg.freq_counter_done").read()
hw.dispatch()
i=0
while int(hex(done),16) == 0:
#    time.sleep(1)
    done = hw.getNode("freq_counter_control_reg.freq_counter_done").read()
    hw.dispatch()
    i += 1
    if i > 10: 
        print('timeout ', i, ' seconds have passed')
        break
print('done: ', hex(done))


### Read the count values and find associated frequencies
freqB = hw.getNode("freq_counter_base").read()
hw.dispatch()
print (type(freqB))
print (freqB)
print('Base clk: ', hex(freqB))


freq0 = hw.getNode("freq_counter_clk0").read()
hw.dispatch()
print('clk 0: ', hex(freq0), ' frequency of ', float(100*int(hex(freq0),16))/float(int(hex(freq0),16)))

freq1 = hw.getNode("freq_counter_clk1").read()
hw.dispatch()
print (type(freq1))
print('clk 1: ', hex(freq1), ' frequency of ', float(100*int(hex(freq1),16))/float(int(hex(freq0),16)))
#print('clk 1: ', freq1, ' frequency of ', 100*float(freq1)/float(freq0))

freq2 = hw.getNode("freq_counter_clk2").read()
hw.dispatch()
print("clk 2: ", hex(freq2), " frequency of ", float(100*int(hex(freq2),16))/float(int(hex(freq0),16)))

freq3 = hw.getNode("freq_counter_clk3").read()
hw.dispatch()
print("clk 3: ", hex(freq3), " frequency of ", float(100*int(hex(freq3),16))/float(int(hex(freq0),16)))

freq4 = hw.getNode("freq_counter_clk4").read()
hw.dispatch()
print("clk 4: ", hex(freq4), " frequency of ", float(100*int(hex(freq4),16))/float(int(hex(freq0),16)))

freq5 = hw.getNode("freq_counter_clk5").read()
hw.dispatch()
print("clk 5: ", hex(freq5), " frequency of ", float(100*int(hex(freq5),16))/float(int(hex(freq0),16)))

freq6 = hw.getNode("freq_counter_clk6").read()
hw.dispatch()
print("clk 6: ", hex(freq6), " frequency of ", float(100*int(hex(freq6),16))/float(int(hex(freq0),16)))

clear = 1
hw.getNode("freq_counter_control_reg.freq_counter_clear").write(clear)
hw.dispatch()
print("writing clear bit: ", clear)

read_clear = hw.getNode("freq_counter_control_reg.freq_counter_clear").read()
hw.dispatch()
print("clear: ", hex(read_clear))
