import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

hw.getNode("control_reg.restart").write(1)
hw.dispatch()

time.sleep(5)


print("\n\nRestarting 10G")

hw.getNode("control_reg.restart").write(0)
hw.dispatch()

time.sleep(5)

