import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
#print("device_id = ", device_id)

deviceuri = hw.uri()
#print("deviceuri = ", deviceuri)

# read current word count
reg1 = hw.getNode("felix_max_words").read()
hw.dispatch()
print ('felix_max_words = ',hex(reg1))

max_words = 32
print ('set max words ',max_words)
# set max words to send
hw.getNode("felix_max_words").write(max_words)
hw.dispatch()

# read current word count
reg1 = hw.getNode("felix_max_words").read()
hw.dispatch()
print ('felix_max_words = ',hex(reg1))



