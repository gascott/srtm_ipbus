import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

#source = int(input('set from 10g source to 0 (prbs) or 1 (user) '))
source = 1
print (source)

hw.getNode("A_control_reg.10g_source").write(source)
hw.dispatch()

reg1 = hw.getNode("A_control_reg.10g_source").read()
hw.dispatch()
print ('from source = ',hex(reg1))



