import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

print("Reading from xfifo_rd_data_lo and rxfifo_rd_data_hi")

for i in range(20):
    reg1 = hw.getNode("rxfifo_rd_data_lo").read()
    hw.dispatch()
    print("low fifo = ", hex(reg1))
    reg2 = hw.getNode("rxfifo_rd_data_hi").read()
    hw.dispatch()
    print("high fifo = ", hex(reg2))
    i += 1

reg4 = hw.getNode("status_reg").read()
hw.dispatch()

reg6 = hw.getNode("status_reg.comp_status").read()
hw.dispatch()

reg5 = hw.getNode("max_words").read()
hw.dispatch()


#hw.getNode("control_reg.restart").write(1)
#hw.dispatch()


print("srtm 10g status reg # = ", hex(reg4))
print("comp_status = ", hex(reg6))
print("max_words_reg = ", reg5)

#print("\n\nRestarting 10G")

#hw.getNode("control_reg.restart").write(0)
#hw.dispatch()

reg8 = hw.getNode("status_reg.re_rxfifo_empty").read()
hw.dispatch()

print("\nstatus_reg.re_rxfifo_empty = ", hex(reg8))

reg0 = hw.getNode("data_lo").read()
hw.dispatch()
reg1 = hw.getNode("data_hi").read()
hw.dispatch()
reg2 = hw.getNode("max_words").read()
hw.dispatch()
reg3 = hw.getNode("control_reg").read() 
hw.dispatch()
reg4 = hw.getNode("status_reg").read()
hw.dispatch()
reg5 = hw.getNode("rxfifo_rd_data_lo").read()
hw.dispatch()
reg6 = hw.getNode("rxfifo_rd_data_hi").read()
hw.dispatch()
reg7 = hw.getNode("wrd_cnt").read()
hw.dispatch()


print("\n\ndata lo reg =       ", hex(reg0))
print("data hi reg =       ", hex(reg1))
print("max words reg =     ", hex(reg2))
print("control reg =       ", hex(reg3))
print("status reg =        ", hex(reg4))
print("rxfifo rd data lo = ", hex(reg5))
print("rxfifo rd data hi = ", hex(reg6))
print("word cnt reg =      ", hex(reg7))


print("srtm test done")
