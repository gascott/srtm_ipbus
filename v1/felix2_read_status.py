import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg = hw.getNode("felix_status_reg").read()
hw.dispatch()
reg0 = hw.getNode("felix_status_reg.flx128_lnk_stat").read()
hw.dispatch()
reg1 = hw.getNode("felix_status_reg.flx130_lnk_stat").read()
hw.dispatch()
reg2 = hw.getNode("felix_status_reg.gtwiz_qpll_lock").read()
hw.dispatch()
reg4 = hw.getNode("felix_status_reg.test_pass").read()
hw.dispatch()
reg5 = hw.getNode("felix_status_reg.ch0_b130_rxbyteisaligned").read()
hw.dispatch()
reg6 = hw.getNode("felix_status_reg.ch0_b130_rxcommadet").read()
hw.dispatch()
reg7 = hw.getNode("felix_status_reg.ch1_b128_tx_comma_active").read()
hw.dispatch()

print (type(reg0))
print("felix status reg = ", hex(reg))
print("flx128_lnk_stat = ", hex(reg0))
print("flx130_lnk_stat = ", hex(reg1))
print("gtwiz_qpll_lock = ", hex(reg2))
print("test_pass       = ", hex(reg4))
print("felix_status_reg.ch0_b130_rxbyteisaligned       = ", hex(reg5))
print("felix_status_reg.ch0_b130_rxcommadet       = ", hex(reg6))
print("felix_status_reg.ch1_b128_tx_comma_active       = ", hex(reg7))

print('done!')
