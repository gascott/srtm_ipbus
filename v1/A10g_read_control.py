import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg0 = hw.getNode("A_control_reg").read()
hw.dispatch()
reg1 = hw.getNode("A_control_reg.send").read()
hw.dispatch()
reg2 = hw.getNode("A_control_reg.restart").read()
hw.dispatch()
reg3 = hw.getNode("A_control_reg.continuous_send").read()
hw.dispatch()
reg4 = hw.getNode("A_control_reg.10g_source").read()
hw.dispatch()
reg5 = hw.getNode("A_control_reg.enable_loopback").read()
hw.dispatch()
reg6 = hw.getNode("A_control_reg.reset").read()
hw.dispatch()

print (type(reg0))
print("srtm 10g control reg = ", hex(reg0))

print("send              =  ", hex(reg1))
print("restart           =  ", hex(reg2))
print("continuous send   =  ", hex(reg3))
print("10g_source        =  ", hex(reg4))
print("enable_loopback   =  ", hex(reg5))
print("reset =  ", hex(reg6))

print('done!')
