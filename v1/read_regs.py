import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg0 = hw.getNode("max_words").read()
hw.dispatch()

reg1 = hw.getNode("control_reg").read()
hw.dispatch()
reg1a = hw.getNode("control_reg.send").read()
hw.dispatch()
reg1b = hw.getNode("control_reg.restart").read()
hw.dispatch()
reg1c = hw.getNode("control_reg.cont_pckt").read()
hw.dispatch()
reg1d = hw.getNode("control_reg.frm_src").read()
hw.dispatch()

reg2 = hw.getNode("status_reg").read()
hw.dispatch()
reg2a = hw.getNode("status_reg.busy").read()
hw.dispatch()
reg2b = hw.getNode("status_reg.gt_locked").read()
hw.dispatch()
reg2c = hw.getNode("status_reg.block_lock").read()
hw.dispatch()
reg2d = hw.getNode("status_reg.comp_status").read()
hw.dispatch()
reg2e = hw.getNode("status_reg.stat_reg_cmp").read()
hw.dispatch()
reg2f = hw.getNode("status_reg.re_rxfifo_empty").read()
hw.dispatch()

reg3 = hw.getNode("wrd_cnt").read()
hw.dispatch()

print("Fresh from Reboot...")

print("\nmax_words = ", reg0)

print("\ncontrol_reg = ", hex(reg1))
print("  send = ", hex(reg1a))
print("  restart = ", hex(reg1b))
print("  cont_pckt = ", hex(reg1c))
print("  frm_src   = ", hex(reg1d)) 

print("\nstatus_reg = ", hex(reg2))
print("  busy =        ", hex(reg2a))
print("  gt_locked =   ", hex(reg2b))
print("  block_lock =  ", hex(reg2c))
print("  comp_status = ", hex(reg2d))
print("  stat_reg_cmp =    ", hex(reg2e))
print("  re_rxfifo_empty = ", hex(reg2f))

print("\nword cnt = ", reg3)

hw.getNode("control_reg.restart").write(1)
hw.dispatch()

reg4 = hw.getNode("control_reg").read()
hw.dispatch()

reg5 = hw.getNode("status_reg").read()
hw.dispatch()

print("\nIn Restart Mode...")
print("\ncontrol_reg = ", hex(reg4))
print("\nstatus_reg  = ", hex(reg5))
time.sleep(2)

hw.getNode("control_reg.restart").write(0)
hw.dispatch()

print("\nBack from Restart...")

reg0 = hw.getNode("max_words").read()
hw.dispatch()

reg1 = hw.getNode("control_reg").read()
hw.dispatch()
reg1a = hw.getNode("control_reg.send").read()
hw.dispatch()
reg1b = hw.getNode("control_reg.restart").read()
hw.dispatch()
reg1c = hw.getNode("control_reg.cont_pckt").read()
hw.dispatch()
reg1d = hw.getNode("control_reg.frm_src").read()
hw.dispatch()

reg2 = hw.getNode("status_reg").read()
hw.dispatch()
reg2a = hw.getNode("status_reg.busy").read()
hw.dispatch()
reg2b = hw.getNode("status_reg.gt_locked").read()
hw.dispatch()
reg2c = hw.getNode("status_reg.block_lock").read()
hw.dispatch()
reg2d = hw.getNode("status_reg.comp_status").read()
hw.dispatch()
reg2e = hw.getNode("status_reg.stat_reg_cmp").read()
hw.dispatch()
reg2f = hw.getNode("status_reg.re_rxfifo_empty").read()
hw.dispatch()

reg3 = hw.getNode("wrd_cnt").read()
hw.dispatch()


print("\nmax_words = ", reg0)

print("\ncontrol_reg = ", hex(reg1))
print("  send = ", hex(reg1a))
print("  restart = ", hex(reg1b))
print("  cont_pckt = ", hex(reg1c))
print("  frm_src   = ", hex(reg1d)) 

print("\nstatus_reg = ", hex(reg2))
print("  busy =        ", hex(reg2a))
print("  gt_locked =   ", hex(reg2b))
print("  block_lock =  ", hex(reg2c))
print("  comp_status = ", hex(reg2d))
print("  stat_reg_cmp =    ", hex(reg2e))
print("  re_rxfifo_empty = ", hex(reg2f))

print("\nword cnt = ", reg3)


print("srtm test done")
