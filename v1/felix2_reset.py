import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reset = 1
print ('set reset to 1')

reg1 = hw.getNode("felix_control_reg.reset").read()
hw.dispatch()
print ('from reset = ',hex(reg1))

hw.getNode("felix_control_reg.reset").write(reset)
hw.dispatch()

reg1 = hw.getNode("felix_control_reg.reset").read()
hw.dispatch()
print ('from reset = ',hex(reg1))

reset = 0
print ('set reset to 0')

reg1 = hw.getNode("felix_control_reg.reset").read()
hw.dispatch()
print ('from reset = ',hex(reg1))

hw.getNode("felix_control_reg.reset").write(reset)
hw.dispatch()

reg1 = hw.getNode("felix_control_reg.reset").read()
hw.dispatch()
print ('from reset = ',hex(reg1))

reg1 = hw.getNode("felix_control_reg").read()
hw.dispatch()
print ('control reg = ',hex(reg1))

print ('done!')

