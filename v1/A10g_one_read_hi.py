import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

# read current word count
reg1 = hw.getNode("A_fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))

# read data
reg4 = hw.getNode("A_rxfifo_rd_data_hi").read()
hw.dispatch()
print ('data_hi read = ',hex(reg4))

# check word count again
reg1 = hw.getNode("A_fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))



