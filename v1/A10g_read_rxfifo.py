import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

maxwords = 256

for i in range(maxwords):

# read data
    reg4 = hw.getNode("A_rxfifo_rd_data_lo").read()
    hw.dispatch()
    print ('data_lo read = ',hex(reg4))

# read data
    reg4 = hw.getNode("A_rxfifo_rd_data_hi").read()
    hw.dispatch()
    print ('data_hi read = ',hex(reg4))

