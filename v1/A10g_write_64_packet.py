import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

#hex_hi = 3735879680 - 1
hex_hi = 3735879680
for i in range(64):
    # write 256 64-bit words
    # word 0
    # dead0000 = 3735879680
    hex_lo = hex_hi + 1
    hex_hi = hex_lo + 1
#hex_lo = '0xdead0000'
#hex_hi = '0xdead0001'
#hex_lo = int(hex_lo,0)
#hex_hi = int(hex_hi,0)
#print (hex_lo)
#print (hex_hi)

#    reg1 = hw.getNode("fifo_word_count").read()
#    hw.dispatch()
#    print ('word count = ',hex(reg1))

    #write two words
    hw.getNode("A_data_to_send").write(hex_lo)
    hw.dispatch()
    hw.getNode("A_data_to_send").write(hex_hi)
    hw.dispatch()
    print ('data written',hex(hex_lo),hex(hex_hi))

    reg1 = hw.getNode("A_fifo_word_count").read()
    hw.dispatch()
    print ('word count = ',hex(reg1))

