import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
#print("device_id = ", device_id)

deviceuri = hw.uri()
#print("deviceuri = ", deviceuri)

# read current word count
reg1 = hw.getNode("A_fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))

#print ('set max words')
#set max words to send
#max_words = 32
#hw.getNode("A_max_words").write(max_words)
#hw.dispatch()

# temp comment out
#reg3 = hw.getNode("A_max_words").read()
#hw.dispatch()
#print ('max_words = ',hex(reg3))

# set source
#print ('set source = 1')
#source = 1
#hw.getNode("A_control_reg.10g_source").write(source)
#hw.dispatch()

# clear mgt loopback enable
#data = 1
#print ('clear mgt loopback enable = ',data)
#hw.getNode("A_mgt_loopback_mdio.enable").write(data)
#hw.dispatch()

# now send
#hw.getNode("A_control_reg.send").write(1)
#hw.dispatch()
#print ('set send to 1')

# now send                                                                     
hw.getNode("A_control_reg").write(0x11) 
hw.dispatch() 
print ('set control_reg to 0x11')

# temp comment out
#time.sleep(5)

# now set back
#hw.getNode("A_control_reg.send").write(0)
#hw.dispatch()
#print ('set send to 0')

# check word count again
#reg1 = hw.getNode("A_fifo_word_count").read()
#hw.dispatch()
#print ('word count = ',hex(reg1))

