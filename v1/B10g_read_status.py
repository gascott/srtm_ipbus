import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg0 = hw.getNode("B_status_reg").read()
hw.dispatch()
reg1 = hw.getNode("B_status_reg.busy").read()
hw.dispatch()
reg2 = hw.getNode("B_status_reg.gt_locked").read()
hw.dispatch()
reg3 = hw.getNode("B_status_reg.block_lock").read()
hw.dispatch()
reg4 = hw.getNode("B_status_reg.completion_status").read()
hw.dispatch()
reg5 = hw.getNode("B_status_reg.statistics_reg_compare").read()
hw.dispatch()
reg6 = hw.getNode("B_status_reg.rd_rxfifo_empty").read()
hw.dispatch()

print (type(reg0))
print("srtm 10g status reg = ", hex(reg0))

print("busy            =  ", hex(reg1))
print("gt_locked       =  ", hex(reg2))
print("block_lock      =  ", hex(reg3))
print("completion_status =  ", hex(reg4))
print("statistics_reg_compare =  ", hex(reg5))
print("rd_rxfifo_empty =  ", hex(reg6))

print('done!')
