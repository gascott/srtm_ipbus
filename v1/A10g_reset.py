import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reset = 1
print ('set reset to 1')

reg1 = hw.getNode("A_control_reg.reset").read()
hw.dispatch()
print ('from reset = ',hex(reg1))

hw.getNode("A_control_reg.reset").write(reset)
hw.dispatch()

reg1 = hw.getNode("A_control_reg.reset").read()
hw.dispatch()
print ('from reset = ',hex(reg1))


