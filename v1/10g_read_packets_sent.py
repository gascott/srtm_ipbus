import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg0 = hw.getNode("sanity_reg").read()
hw.dispatch()
print("sanity check register = ", hex(reg0))

reg0 = hw.getNode("packet_counter_register").read()
hw.dispatch()
print("packet counter reg  = ", hex(reg0))

print('done!')
