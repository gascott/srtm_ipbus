import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

#source = int(input('set from 10g source to 0 (prbs) or 1 (user) '))
source = 1
print (source)

hw.getNode("A_control_reg.enable_loopback").write(source)
hw.dispatch()

reg1 = hw.getNode("A_control_reg.enable_loopback").read()
hw.dispatch()
print ('A10 enable loopback = ',hex(reg1))

reg1 = hw.getNode("A_control_reg").read()
hw.dispatch()
print ('A10 control_reg = ',hex(reg1))


