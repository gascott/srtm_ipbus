import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

hex_lo = '0xc0ffee00'
hex_hi = '0xc0ffee01'
hex_lo = int(hex_lo,0)
hex_hi = int(hex_hi,0)
print (hex_lo)
print (hex_hi)

reg1 = hw.getNode("fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))

#write two words
hw.getNode("data_to_send").write(hex_lo)
hw.dispatch()
hw.getNode("data_to_send").write(hex_hi)
hw.dispatch()

reg1 = hw.getNode("fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))



