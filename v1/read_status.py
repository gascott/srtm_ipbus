import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg4 = hw.getNode("status_reg").read()
hw.dispatch()

reg6 = hw.getNode("status_reg.comp_status").read()
hw.dispatch()

reg5 = hw.getNode("max_words").read()
hw.dispatch()


print("srtm 10g status reg # = ", hex(reg4))
print("comp_status = ", hex(reg6))
print("max_words_reg = ", reg5)


print("srtm test done")
