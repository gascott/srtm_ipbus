import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
#print("device_id = ", device_id)

deviceuri = hw.uri()
#print("deviceuri = ", deviceuri)

# read current word count
reg1 = hw.getNode("A_fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))

max_words = 256
print ('set max words ',max_words)
# set max words to send
hw.getNode("A_max_words").write(max_words)
hw.dispatch()

