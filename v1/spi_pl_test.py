import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

mstr_tx_data = 0xdeadbeef
slv_tx_data = 0xc0ffeeee

# write data to tx fifo for master and slave and read word count
hw.getNode("spi_master_txrx_fifo").write(mstr_tx_data)
hw.dispatch()

mstr_wrd_cnt = hw.getNode("spi_master_tx_wrd_cnt").read()
hw.dispatch()

print(f"Master Fifo Word count: {mstr_wrd_cnt}")

hw.getNode("spi_slave_txrx_fifo").write(slv_tx_data)
hw.dispatch()

slv_wrd_cnt = hw.getNode("spi_slave_tx_wrd_cnt").read()
hw.dispatch()

print(f"Slave Fifo Word count: {slv_wrd_cnt}")

master_read_data = hw.getNode("spi_master_txrx_fifo").read()
hw.dispatch()
slave_read_data = hw.getNode("spi_slave_txrx_fifo").read()
hw.dispatch()

print(f"Read Master FIFO: {hex(master_read_data)}")
print(f"Read Slave FIFO:  {hex(slave_read_data)}")


# Send data 
hw.getNode("spi_slv_cntr.send").write(1)
hw.dispatch()
hw.getNode("spi_mstr_cntr.send").write(1)
hw.dispatch()

time.sleep(1)

mstr_rx_wrd_cnt = hw.getNode("spi_master_rx_wrd_cnt").read()
hw.dispatch()
slv_rx_wrd_cnt = hw.getNode("spi_slave_rx_wrd_cnt").read()
hw.dispatch()

print(f"Master Rx Word Count: {mstr_rx_wrd_cnt}")
print(f"Slave Rx Word Count: {slv_rx_wrd_cnt}")

master_read_data = hw.getNode("spi_master_txrx_fifo").read()
hw.dispatch()
slave_read_data = hw.getNode("spi_slave_txrx_fifo").read()
hw.dispatch()

print(f"Read Master FIFO: {hex(master_read_data)}")
print(f"Read Slave FIFO:  {hex(slave_read_data)}")

# Send data 
hw.getNode("spi_slv_cntr.reset_fifo").write(1)
hw.dispatch()
hw.getNode("spi_mstr_cntr.reset_fifo").write(1)
hw.dispatch()

mstr_rx_wrd_cnt = hw.getNode("spi_master_rx_wrd_cnt").read()
hw.dispatch()
slv_rx_wrd_cnt = hw.getNode("spi_slave_rx_wrd_cnt").read()
hw.dispatch()

print(f"Master Rx Word Count: {mstr_rx_wrd_cnt}")
print(f"Slave Rx Word Count: {slv_rx_wrd_cnt}")

# hw.getNode("spi_slave_cntr_set_send").write(1)
# hw.dispatch()
# hw.getNode("spi_master_cntr_set_send").write(1)
# hw.dispatch()

# time.sleep(1)

# mstr_rx_wrd_cnt = hw.getNode("spi_master_rx_wrd_cnt").read()
# hw.dispatch()
# slv_rx_wrd_cnt = hw.getNode("spi_slave_rx_wrd_cnt").read()
# hw.dispatch()

# print(f"Master Rx Word Count: {mstr_rx_wrd_cnt}")
# print(f"Slave Rx Word Count: {slv_rx_wrd_cnt}")


