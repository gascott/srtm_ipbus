import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

# read current word count
reg1 = hw.getNode("wrd_cnt").read()
hw.dispatch()
print ('word count = ',hex(reg1))

# set source and read source
hw.getNode("control_reg.10g_source").write(1)
hw.dispatch()
print ('set 10g_source = 1')

#reg2 = hw.getNode("control_reg.10g_source").read()
hw.dispatch()
print ('read 10g source = ',hex(reg2))

# set max words to send
max_words = 16
hw.getNode("max_words").write(max_words)
hw.dispatch()

reg3 = hw.getNode("max_words").read()
hw.dispatch()
print ('max_words = ',hex(reg3))

# now send
hw.getNode("control_reg.send").write(1)
hw.dispatch()
print ('set send to 1')

time.sleep(20)

# now set back
hw.getNode("control_reg.send").write(0)
hw.dispatch()
print ('set send to 0')

# check word count again
reg1 = hw.getNode("fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))

# read one word of data
reg4 = hw.getNode("rxfifo_rd_data_lo").read()
hw.dispatch()
print ('data_lo read = ',hex(reg4))

# check word count again
reg1 = hw.getNode("fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))

# read one word of data
reg4 = hw.getNode("rxfifo_rd_data_hi").read()
hw.dispatch()
print ('data_hi read = ',hex(reg4))

# check word count again
reg1 = hw.getNode("fifo_word_count").read()
hw.dispatch()
print ('word count = ',hex(reg1))



