import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

high = 1
low = 0

hw.getNode("B_mgt_loopback_mdio.enable").write(low)
hw.dispatch()

reg1 = hw.getNode("B_mgt_loopback_mdio.enable").read()
hw.dispatch()
print ('mgt loopback mdio enable = ',hex(reg1))

hw.getNode("B_mgt_loopback_mdio.enable").write(high)
hw.dispatch()

reg1 = hw.getNode("B_mgt_loopback_mdio.enable").read()
hw.dispatch()
print ('mgt loopback mdio enable = ',hex(reg1))


