import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

reg1 = hw.getNode("felix_control_reg").read()
hw.dispatch()
print ('felix_control_reg = ',hex(reg1))

reset = 1
print ('set frm_src to 1')

reg1 = hw.getNode("felix_control_reg.frame_source").read()
hw.dispatch()
print ('from frame_source = ',hex(reg1))

hw.getNode("felix_control_reg.frame_source").write(reset)
hw.dispatch()

reg1 = hw.getNode("felix_control_reg.frame_source").read()
hw.dispatch()
print ('from frame_source = ',hex(reg1))

reg1 = hw.getNode("felix_control_reg").read()
hw.dispatch()
print ('felix_control_reg = ',hex(reg1))

print ('done!')

