import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
print("device_id = ", device_id)

deviceuri = hw.uri()
print("deviceuri = ", deviceuri)

restart = 1
print ('set restart to 1')

hw.getNode("A_control_reg.restart").write(restart)
hw.dispatch()

reg1 = hw.getNode("A_control_reg.restart").read()
hw.dispatch()
print ('from restart = ',hex(reg1))

restart = 0
print ('set restart to 0')

hw.getNode("A_control_reg.restart").write(restart)
hw.dispatch()

reg1 = hw.getNode("A_control_reg.restart").read()
hw.dispatch()
print ('from restart = ',hex(reg1))



