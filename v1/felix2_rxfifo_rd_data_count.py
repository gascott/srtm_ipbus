import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel. WARNING)

manager = uhal.ConnectionManager("file://srtm_connection.xml") 
hw = manager.getDevice("udp.srtm")

device_id = hw.id()
#print("device_id = ", device_id)

deviceuri = hw.uri()
#print("deviceuri = ", deviceuri)

reg1 = hw.getNode("felix_rxfifo_rd_data_count").read()
hw.dispatch()
print ('felix_rxfifo_rd_data_count = ',hex(reg1))

